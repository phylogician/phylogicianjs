import { Phylogician } from '../src/Phylogician'
import { Tree } from '../src/Tree'

describe('Phylogician', () => {
    describe('draw end2end test', () => {
        const newick = '((Alice:2,Debbie:3):4,((Bob:3,Charlie:4):2,Jack:5):3);'
        describe('default - vertical and scaling', () => {
            beforeAll(async () => {
                const pageSize = {
                    height: 1000,
                    width: 1000
                }
                await page.setViewport(pageSize)
                await page.setContent(`<html><head></head><body><h1><center>PhylogicianJS development environment.</center></h1><div id='TreeDiv'></div><button id='scale' onclick="clicked()">scale</button><button id='makeTree' onclick="makeTree()">makeTree</button></body></html>`)
                await page.addStyleTag({
                    content: `
                    body {
                        margin-top: 30px;
                    }
                    #TreeDiv {
                        width: 800px;
                        height: 500px;
                        border-width: 1px;
                        border-color: black;
                        border-style: solid;
            
                        position:absolute; /*it can be fixed too*/
                        left:0; right:0;
                        top:0; bottom:0;
                        margin:auto;
                    }`
                })
                await page.addScriptTag({
                    path: './dev/index.js'
                })
                await page.addScriptTag({
                    content: `
                        function makeTree () {
                            phyloTree.data("${newick}").setLayout('vertical').draw("#TreeDiv")
                        }
                        function clicked () {
                            phyloTree.toggleScale()
                        }`
                })
                await page.click('#makeTree')
                await page.waitFor(2000)
            })
            test('It should have calculated Alice\'s label position correctly', async () => {
                const alice = await page.evaluate(() => document.getElementById('phylogicianJS-label-2'))
                expect(alice).toMatchSnapshot()
            })
            test('It should have calculated Charlie\'s label position correctly', async () => {
                const charlie = await page.evaluate(() => document.getElementById('phylogicianJS-label-2'))
                expect(charlie).toMatchSnapshot()
            })
            test('It should have draw Alice\'s label position correctly', async () => {
                const expectedBBox =  { width: 34.78125, height: 17 }
                const alice = await page.$('#phylogicianJS-label-2')
                expect(alice).not.toBeNull()
                if (alice) {
                    const bBox = await alice.boundingBox()
                    expect(bBox).not.toBeNull()
                    if (bBox) {
                        expect(bBox.width/10).toBeCloseTo(expectedBBox.width/10, 1)
                        expect(bBox.height/100).toBeCloseTo(expectedBBox.height/100, 1)
                    }
                }
            })
            test('It should have draw Charlie\'s label position correctly', async () => {
                const expectedBBox =  { width: 47.09375, height: 18 }
                const charlie = await page.$('#phylogicianJS-label-7')
                expect(charlie).not.toBeNull()
                if (charlie) {
                    const bBox = await charlie.boundingBox()
                    expect(bBox).not.toBeNull()
                    if (bBox) {
                        expect(bBox.width/10).toBeCloseTo(expectedBBox.width/10, 1)
                        expect(bBox.height/100).toBeCloseTo(expectedBBox.height/100, 1)
                    }
                }
            })
            test('It should have draw the nodes with the right radius', async () => {
                const aliceRealNodeRadius = await page.evaluate( () => {
                    const aliceN = document.getElementById('phylogicianJS-node-2')
                    if(aliceN) {
                        return parseInt(aliceN.getAttribute('r') || '', 10)
                    }
                })
                const alice = await page.$('#phylogicianJS-node-2')
                expect(alice).not.toBeNull()
                if (alice) {
                    const aliceElement = await alice.jsonValue()
                    const aliceExpectedNodeRadius = aliceElement.__data__.viz.node.size
                    expect(aliceRealNodeRadius).toEqual(aliceExpectedNodeRadius)
                }
            })
            test('It should have all the y positions of leafs equidistant from each other', async () => {
                const tree = new Tree()
                tree.buildTree(newick)
                const nodeSvgIds = tree.getLeafIds().map((n) => `phylogicianJS-node-${tree.getNode(n).id}`)
                const nodesPosX = nodeSvgIds.map(async (nodeSvgId) => {
                    return page.evaluate((nId) => {
                        const n = document.getElementById(nId)
                        if (n) {
                            return parseInt(n.getAttribute('cy') || '', 10)
                        }
                        else {
                            return null
                        }
                    },nodeSvgId)
                })
                return Promise.all(nodesPosX).then((results) => {
                    const diff = results.map((x, i) => {
                        if (x === null) {
                            return x
                        }
                        if (i && x !== null) {
                            const oldValue = results[i - 1]
                            if (oldValue !== null) {
                                return x - oldValue
                            } else {
                                return null
                            }
                        }
                        return null
                    })
                    diff.shift()
                    diff.forEach((r) => {
                        expect(r).not.toBeUndefined()
                    })
                    const allEqual = diff.every((x, i, arr) => x === arr[0])
                    expect(diff.length).toEqual(nodeSvgIds.length  - 1)
                    expect(allEqual).toBeTruthy()
                })
            })
            // test('Labels should be move as nodes change size')
            describe('scaled', () => {
                beforeAll(async() => {
                    await page.click('#scale')
                    await page.waitFor(1000)
                })
                test('It should have draw all leafs at the same x position after scaling', async () => {
                    const tree = new Tree()
                    tree.buildTree(newick)
                    const nodeSvgIds = tree.getLeafIds().map((n) => `phylogicianJS-node-${tree.getNode(n).id}`)
                    const nodesPosX = nodeSvgIds.map(async (nodeSvgId) => {
                        return page.evaluate((nId) => {
                            const n = document.getElementById(nId)
                            if (n) {
                                return parseInt(n.getAttribute('cx') || '', 10)
                            }
                            else {
                                return null
                            }
                        },nodeSvgId)
                    })
                    return Promise.all(nodesPosX).then((results) => {
                        const allEqual = results.every((x, i, arr) => x === arr[0])
                        results.forEach((r) => {
                            expect(r).not.toBeUndefined()
                        })
                        expect(results.length).toEqual(nodeSvgIds.length)
                        expect(allEqual).toBeTruthy()
                    })
                })
            })
        })
    })
})