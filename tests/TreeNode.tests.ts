import { TreeNode } from '../src/TreeNode'

describe('TreeNode', () => {
    describe('constructor', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id: number = 0
        const node = new TreeNode(name, branchLength, id)
        test('it should have the property id', () => {
            expect(node.id).toEqual(id)
        })
        test('it should have the property name', () => {
            expect(node.name).toEqual(name)
        })
        test('children should be empty array', () => {
            expect(node.children).toEqual([])
        })
        test('it should have the property branchLength', () => {
            expect(node.branchLength).toEqual(branchLength)
        })
        test('the property parentNodeId should be null', () => {
            const parentNodeId = node.getParentNodeId()
            expect(parentNodeId).toBeNull()
        })
    })
    describe('setParentNodeId & getParentNodeId', () => {
        const name:string = 'node'

        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        test('it should set parent', () => {
            const parentNodeId = 2
            node.setParentNodeId(parentNodeId)
            const currentParentNodeId = node.getParentNodeId()
            expect(currentParentNodeId).toEqual(parentNodeId)
        })
        test('it should allow to set the parent again - for reroot for example', () => {
            const newParentNodeId = 1
            node.setParentNodeId(newParentNodeId)
            const currentParentNodeId = node.getParentNodeId()
            expect(currentParentNodeId).toEqual(newParentNodeId)
        })
    })
    describe('isLeaf', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        test('it should return true if there is no children', () => {
            const isLeaf = node.isLeaf()
            expect(isLeaf).toBeTruthy()
        })
        test('it should return false if there is at least one children', () => {
            node.children.push(1)
            const isLeaf = node.isLeaf()
            expect(isLeaf).toBeFalsy()
        })
    })
    describe('isRoot, setRoot and unsetRoot', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        test('it should return false by default', () => {
            const isRoot = node.isRoot()
            expect(isRoot).toBeFalsy()
        })
        test('it should return true if the node is set as root', () => {
            node.setAsRoot()
            const isRoot = node.isRoot()
            expect(isRoot).toBeTruthy()
        })
        test('it should return false if the node is unset as root', () => {
            node.unsetAsRoot()
            const isRoot = node.isRoot()
            expect(isRoot).toBeFalsy()
        })
    })
    describe('setNodeSize & getNodeSize', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        test('it should set node size', () => {
            const nodeSize = 2
            node.setNodeSize(nodeSize)
            const currentNodeSize = node.getNodeSize()
            expect(currentNodeSize).toEqual(nodeSize)
        })
    })
    describe('setNodeColor & getNodeColor', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        test('it should set node size', () => {
            const newNodeColor = 'red'
            node.setNodeColor(newNodeColor)
            const currentNodeColor = node.getNodeColor()
            expect(currentNodeColor).toEqual(newNodeColor)
        })
    })
    describe('setPosY & getPosY', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        test('it should set node size', () => {
            const newPosY = 10
            node.setPosY(newPosY)
            const currentPosY = node.getPosY()
            expect(currentPosY).toEqual(newPosY)
        })
    })
    describe('setPosX & getPosX', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        test('it should set node size', () => {
            const newPosX = 10
            node.setPosX(newPosX)
            const currentPosX = node.getPosX()
            expect(currentPosX).toEqual(newPosX)
        })
    })
    describe('setLabelWidth & getLabelWidth', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        test('it should set node size', () => {
            const newLabelWidth = 10
            node.setLabelWidth(newLabelWidth)
            const currentLabelWidth = node.getLabelWidth()
            expect(currentLabelWidth).toEqual(newLabelWidth)
        })
    })
    describe('setLabelHeight & getLabelHeight', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        test('it should set node size', () => {
            const newLabelHeight = 10
            node.setLabelHeight(newLabelHeight)
            const currentLabelHeight = node.getLabelHeight()
            expect(currentLabelHeight).toEqual(newLabelHeight)
        })
    })

    describe('setBranchPath & getBranchPath', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        test('it should set node size', () => {
            const newBranchPath = 'M20,235.625L20,77.5L336.84722222222223,77.5'
            node.setBranchPath(newBranchPath)
            const currentBranchPath = node.getBranchPath()
            expect(currentBranchPath).toEqual(newBranchPath)
        })
    })
})
