import { Tree } from '../src/Tree'

describe('Tree', () => {
    describe('data', () => {
        describe('with simple newick', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const tree = new Tree()
            tree.buildTree(nwk)
            test('tree array must have node ids in order', () => {
                const idArray = tree.nodes.map((node) => {
                    return node.id
                })
                const expected = [0, 1, 2, 3, 4]
                expect(idArray).toEqual(expected)
            })
            test('tree array must have the correct branch lengths', () => {
                const branchLengthArray = tree.nodes.map((node) => {
                    return node.branchLength
                })
                const expected = [null, 2, 5, 3, 4]
                expect(branchLengthArray).toEqual(expected)
            })
            test('should set the right parents', () => {
                const parentNodeIds = tree.nodes.map((node) => node.getParentNodeId())
                const expected = [null, 0, 0, 2, 2]
                expect(parentNodeIds).toEqual(expected)
            })
        })
        describe('with complex newick', () => {
            const nwk = '((Alice:2,Debbie:3):4,((Bob:3,Charlie:4):2,Ellen:5):3);'
            const tree = new Tree()
            tree.buildTree(nwk)
            test('tree array must have node ids in order', () => {
                const idArray = tree.nodes.map((node) => {
                    return node.id
                })
                const expected = [0, 1, 2, 3, 4, 5, 6, 7, 8]
                expect(idArray).toEqual(expected)
            })
            test('tree array must have the correct branch lengths', () => {
                const branchLengthArray = tree.nodes.map((node) => {
                    return node.branchLength
                })
                const expected = [null, 4, 2, 3, 3, 2, 3, 4, 5]
                expect(branchLengthArray).toEqual(expected)
            })
            test('should set the right parents', () => {
                const parentNodeIds = tree.nodes.map((node) => node.getParentNodeId())
                const expected = [null, 0, 1, 1, 0, 4, 5, 5, 4]
                expect(parentNodeIds).toEqual(expected)
            })
        })
    })

    describe('getNumberOfLeafs', () => {
        test('should count leaves', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const expected = 3
            const tree = new Tree()
            tree.buildTree(nwk)
            const nLeaves = tree.getNumberOfLeafs()
            expect(nLeaves).toEqual(expected)
        })
    })
    describe('findNodeByName', () => {
        test('Must find the right node by name', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const selectNodeName = 'B'
            const tree = new Tree()
            tree.buildTree(nwk)
            const selectedNode = tree.findNodeByName(selectNodeName)
            expect(selectedNode.length).toEqual(1)
            const node = selectedNode[0]
            expect(node.name).toEqual(selectNodeName)
            expect(node.id).toEqual(3)
        })
    })
    describe('getNode', () => {
        test('should get the correct node', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const selectNodeId = 3
            const tree = new Tree()
            tree.buildTree(nwk)
            const selectedNode = tree.getNode(selectNodeId)
            expect(selectedNode.id).toEqual(selectNodeId)
            expect(selectedNode.name).toEqual('B')
        })
        test('should throw if there is no node with that Id', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const selectNodeId = 10
            const tree = new Tree()
            tree.buildTree(nwk)
            expect(() => {
                tree.getNode(selectNodeId)
            }).toThrow(`Id 10 not found`)
        })
        test('should throw if there are more than one node with same Id', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const tree = new Tree()
            tree.buildTree(nwk)
            const secondTree = new Tree()
            secondTree.buildTree(nwk)
            const secondTreeNodeId = 1
            const nodeSecondTree = secondTree.getNode(secondTreeNodeId)
            tree.nodes.push(nodeSecondTree)
            expect(() => {
                tree.getNode(secondTreeNodeId)
            }).toThrow(`More than one node with the same Id 1. Something is wrong.`)
        })
    })
    describe('getMaxDistanceToRoot', () => {
        test('should get the correct max distance to root - sum of branch lengths', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const expectedMaxDistanceToRoot = 9
            const tree = new Tree()
            tree.buildTree(nwk)
            const maxDistanceToRoot = tree.getMaxDistanceToRoot()
            expect(maxDistanceToRoot).toEqual(expectedMaxDistanceToRoot)
        })
        test('should get the correct max distance to root - sum of branch lengths - with complex tree', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedMaxDistanceToRoot = 4 + 2 + 3 + 2 + 3
            const tree = new Tree()
            tree.buildTree(nwk)
            const maxDistanceToRoot = tree.getMaxDistanceToRoot()
            expect(maxDistanceToRoot).toEqual(expectedMaxDistanceToRoot)
        })
    })
    describe('getMaxHopsToRoot', () => {
        test('should get the correct max number of hops (internal nodes) to root', () => {
            const nwk = '((Alice:2,Debbie:3):4,((Bob:3,Charlie:4):2,Ellen:5):3);'
            const expectedMaxHopsToRoot = 3
            const tree = new Tree()
            tree.buildTree(nwk)
            const maxHopsToRoot = tree.getMaxHopsToRoot()
            expect(maxHopsToRoot).toEqual(expectedMaxHopsToRoot)
        })
    })
    describe('getAllMaxHopsToChildLeafs', () => {
        test('should get the correct max number of hops (internal nodes) to root', () => {
            const nwk = '((Alice:2,Debbie:3):4,((Bob:3,Charlie:4):2,Ellen:5):3);'
            const expected = [3, 1, 0, 0, 2, 1, 0, 0, 0]
            const tree = new Tree()
            tree.buildTree(nwk)
            const maxHopsToRoot = tree.getAllMaxHopsToChildLeafs()
            expect(maxHopsToRoot).toEqual(expected)
        })
        test('should get the correct max number of hops (internal nodes) to root with complicated tree', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expected = [5, 1, 0, 0, 4, 3, 0, 0, 2, 1, 0, 0, 0, 0]
            const tree = new Tree()
            tree.buildTree(nwk)
            const maxHopsToRoot = tree.getAllMaxHopsToChildLeafs()
            expect(maxHopsToRoot).toEqual(expected)
        })
    })
    describe('getRootNode', () => {
        it('should grab the (correct) root node', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedRootName = '0N'
            const tree = new Tree()
            tree.buildTree(nwk)
            const rootNode = tree.getRootNode()
            expect(rootNode).not.toBeNull()
            if (rootNode) {
                expect(rootNode.name).toEqual(expectedRootName)
            }
        })
        it('should throw if more than 1 root is assigned', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const tree = new Tree()
            tree.buildTree(nwk)
            tree.getNode(1).setAsRoot()
            expect(() => tree.getRootNode()).toThrow('There are 2 roots with ids [0, 1] assigned.')
        })
    })
    describe('selectInternalIds', () => {
        it('should work', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedInternalNodesIds = [0, 1, 4, 5, 8, 9]
            const tree = new Tree()
            tree.buildTree(nwk)
            const internalIds = tree.selectInternalIds()
            expect(internalIds).toEqual(expectedInternalNodesIds)
        })
    })
    describe('setAsRoot', () => {
        it('should let you pick a different root', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedInternalNodesIds = [0, 1, 4, 5, 8, 9]
            const tree = new Tree()
            tree.buildTree(nwk)
            const currentRoot = tree.getRootNode()
            expect(currentRoot).not.toBeNull()
            if (currentRoot) {
                expect(currentRoot.id).toEqual(0)
            }
            tree.setAsRoot(1)
            const newRoot = tree.getRootNode()
            expect(newRoot).not.toBeNull()
            if (newRoot) {
                expect(newRoot.id).toEqual(1)
            }
            const oldRoot = tree.getNode(0)
            expect(oldRoot.isRoot()).toBeFalsy()
        })
    })
})
