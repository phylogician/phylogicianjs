# PhylogicianJS

Node.js module to visualize phylogenetic trees.


## Development rational

### Phylogician class responsibility distribution

The `Phylogician` class represents the tree itself.

We can feed the `Phylogician` class with newick data using `.data()`. `Phylogician` then parse the newick using `newick-reader` npm package.

The `newick-reader` returns a deeply nested object with the tree structure.

We decided to flatten this scruture and store each tree node as an array of `TreeNode`s. `Phylogician` does that with `.makeTree()` method.

This makes sense, right? A phylogenetic tree is a collection of nodes. Each node should hold all the information to place it on a abstract version of the tree, for example an array.

Thus, an element of the `TreeNode` class has the following information:

```typescript
public name: string
public branchLength: number|null
public children: number[]

private id: number|null
private parent: number | null
private root: boolean

private viz: IvizOptions
```

We do not exposed `id`, `parent` and `root` directly because they are attributes assigned during the construction of the tree by the `Phylogician` method `.makeTree()`. That information is needed because we flattened the nested structure of the original `ITree` object  To access these values, there are getters and setters methods in the instances of the `TreeNode` class. They are designed to protect the developer from accidentally re-assing these values. In particular `id` that should be only assigned once. `parent` and `root` can be reassinged during reroot for example.

The `TreeNode` is self contained and our philosophy is that each instance of the class should have all information about the node it represents in the phylogenetic tree. For that, `TreeNode` also have the private property `viz` with the following interface:

```typescript
interface Ipos {
    x: number
    y: number
}

interface Idim {
    h: number
    w: number
}

interface IvizOptions {
    nodeSize: number
    nodeColor: string
    pos: Ipos,
    labelDim: Idim,
}
```

The `IvizOptions` store the size of the node, its color, position and dimensions of the label. The `TreeNode` class has the `nodeSize` and `nodeColor` set to `3` and `black` as default. Both `pos` and `labelDim` are set to `0` both for `x`, `y` in `pos` and `h`, `m` in `labelDim`.

These attributes are private but can be set and get with the appropriate public methods.

Ok, now that we have the tree on an abstract set that can be publically accessed by the `Phylogician` attribute `.nodes`, it is time to pick a representation with `.layout()`. This is when Phylogician set the `labelDim` and `pos` attributes of each node before it draws it.

There is only `vertical` layout available right now.


 and how to represent this data by picking a layout using `.layout()`.

> Only `vertical` layout is available.

So the tree itself is a collection of `TreeNode`s and a `Layout`.

This helps us to distribute responsibility between the classes.

For instance, `TreeNode` has:

TODO:

- [ ] Fix CI https://medium.com/@ali.dev/how-to-setup-chrome-headless-on-gitlab-ci-with-puppeteer-docker-fbb562cbaee1
- [ ] Tooltip builder (its own class)
  - [ ] Laderizer
  - [ ] branch opacity based on uncertainty
  - [ ] collapse
- [ ] Resolve overlapping names.
- [ ] ReadMe