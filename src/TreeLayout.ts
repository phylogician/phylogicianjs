import * as d3 from 'd3';
import { Tree } from './Tree';

interface IsvgParams {
	width: number;
	height: number;
	margin: number;
}

abstract class TreeLayout {
	public tree: Tree;
	public svgParams: IsvgParams;

	constructor(tree: Tree, svgParams: IsvgParams) {
		this.tree = tree;
		this.svgParams = svgParams;
	}

	public abstract calcTree(scaling: boolean): void;
	public abstract calcPosY(): TreeLayout;
	public abstract calcPosX(scaling: boolean): TreeLayout;
	public abstract calcBranchPath(): TreeLayout;

	public calcLabelDims(): TreeLayout {
		this.tree.nodes.forEach(node => {
			const svg = d3.select('body').append('svg');

			const text = svg
				.append('text')
				.attr('class', 'labeltmp')
				.text(node.name);

			const domEl = text.node();
			if (!domEl) {
				throw new Error('Cannot find any DOM element with this selection.');
			}
			const dim = domEl.getBBox();
			const h: number = dim.height;
			const w: number = dim.width;
			node.setLabelHeight(h);
			node.setLabelWidth(w);
			svg.remove();
		});
		return this;
	}

	public getLongestLabel(): number {
		let longestLabel = 0;
		this.tree.nodes.forEach(node => {
			const labelWidth = node.getLabelWidth();
			longestLabel = longestLabel < labelWidth ? labelWidth : longestLabel;
		});
		return longestLabel;
	}
}

export { IsvgParams, TreeLayout };
