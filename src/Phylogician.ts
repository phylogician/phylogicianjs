// tslint:disable: no-console
import * as d3 from 'd3';

import { Tree } from './Tree';
import { IsvgParams } from './TreeLayout';
import { TreeNode } from './TreeNode';
import { Vertical } from './VerticalTreeLayout';

interface Ioptions {
	node: {
		hover: {
			fill: string;
			opacity: number;
			radiusMultiplier: number;
			stroke: string;
		};
		select: {
			fill: string;
			opacity: number;
			radiusMultiplier: number;
			stroke: string;
		};
	};
}

const defaultOptions = {
	node: {
		hover: {
			fill: 'orange',
			opacity: 0.5,
			radiusMultiplier: 3,
			stroke: 'none',
		},
		select: {
			fill: 'orange',
			opacity: 1,
			radiusMultiplier: 3,
			stroke: 'red',
		},
	},
};

export class Phylogician {
	public tree: Tree;
	public options: Ioptions;
	private svgParams: IsvgParams;
	private divId: string;
	private scale: boolean;
	private layoutType: string;
	private layout: Vertical;

	/**
	 * Creates an instance of Phylogician.
	 * @memberof Phylogician
	 */
	constructor(options: Ioptions = defaultOptions) {
		this.options = options;
		this.tree = new Tree();
		this.divId = '';
		this.svgParams = {
			height: 500,
			margin: 20,
			width: 800,
		};
		this.scale = true;
		this.layoutType = 'vertical';
		this.layout = new Vertical(this.tree, this.svgParams);
	}

	/**
	 * Builds a Tree from newick.
	 *
	 * @param {string} data
	 * @returns {Phylogician}
	 * @memberof Phylogician
	 */
	public data(data: string): Phylogician {
		this.tree.buildTree(data);
		return this;
	}

	/**
	 * Set positions of nodes and labels according to a layout type.
	 *
	 * @param {string} layoutType
	 * @returns {Phylogician}
	 * @memberof Phylogician
	 */
	public setLayout(layoutType: string): Phylogician {
		switch (layoutType) {
			default: {
				this.layoutType = layoutType;
				this.layout = new Vertical(this.tree, this.svgParams);
				this.layout.calcTree(this.scale);
				break;
			}
		}
		return this;
	}

	/**
	 * Draws tree in browser
	 *
	 * @param {string} divId
	 * @memberof Phylogician
	 */
	public draw(divId: string): Phylogician {
		console.log(`drawing tree in ${divId}`);
		this.divId = divId;
		const svg = d3
			.selectAll(divId)
			.append('svg')
			.attr('width', this.svgParams.width)
			.attr('height', this.svgParams.height)
			.attr('id', 'phylogician');

		const nodes = svg
			.selectAll('g')
			.data(this.tree.nodes)
			.enter()
			.append('g')
			.attr('id', (d: TreeNode) => d.id);

		nodes
			.append('circle')
			.attr('class', 'node')
			.attr('id', (d: TreeNode) => `phylogicianJS-node-${d.id}`)
			.attr('r', (d: TreeNode) => d.getNodeSize())
			.attr('cx', (d: TreeNode) => d.getPosX())
			.attr('cy', (d: TreeNode) => d.getPosY())
			.attr('fill', (d: TreeNode) => d.getNodeColor())
			.attr('opacity', (d: TreeNode) => (d.isLeaf() ? 1 : 0))
			.on('mouseover', this.nodeHoverIn)
			.on('mouseout', this.nodeHoverOut);

		nodes
			.append('path')
			.attr('class', 'branch')
			.attr('id', (d: TreeNode) => `phylogicianJS-branch-${d.id}`)
			.attr('d', (d: TreeNode) => d.getBranchPath())
			.attr('width', (d: TreeNode) => d.getNodeSize())
			.attr('stroke', (d: TreeNode) => d.getNodeColor())
			.attr('fill', 'none');

		nodes
			.append('text')
			.attr('class', 'label')
			.attr('id', (d: TreeNode) => `phylogicianJS-label-${d.id}`)
			.attr('x', (d: TreeNode) => d.getPosX() + 10)
			.attr('y', (d: TreeNode) => d.getPosY() + this.calcLabelHeighOffSet(d))
			.text((d: TreeNode) => d.name);

		return this;
	}

	public update(): Phylogician {
		d3.selectAll('.node')
			.data(this.tree.nodes)
			.transition()
			.duration(300)
			.attr('cx', (d: TreeNode) => d.getPosX());

		d3.selectAll('.label')
			.data(this.tree.nodes)
			.transition()
			.duration(300)
			.attr('x', (d: TreeNode) => d.getPosX() + 5);

		d3.selectAll('.branch')
			.data(this.tree.nodes)
			.transition()
			.duration(300)
			.attr('d', (d: TreeNode) => d.getBranchPath());

		return this;
	}

	public toggleScale(): Phylogician {
		console.log('pressed');
		this.scale = this.scale ? false : true;
		this.layout.calcPosX(this.scale).calcBranchPath();
		this.update();
		return this;
	}

	private calcLabelHeighOffSet(node: TreeNode): number {
		return node.getLabelHeight() / 4;
	}

	private nodeHoverIn = (node: TreeNode) => {
		d3.selectAll(`#phylogicianJS-node-${node.id}`)
			.attr('r', () => {
				return node.getNodeSize() * this.options.node.hover.radiusMultiplier;
			})
			.attr('fill', this.options.node.hover.fill)
			.attr('stroke', this.options.node.hover.stroke)
			.attr('opacity', 0.5);
	};

	private nodeHoverOut = (node: TreeNode) => {
		d3.select(`#phylogicianJS-node-${node.id}`)
			.attr('r', node.getNodeSize())
			.attr('fill', node.getNodeColor())
			.attr('opacity', node.isLeaf() ? 1 : 0);
	};
}
