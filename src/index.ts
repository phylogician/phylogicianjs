import { Phylogician } from './Phylogician';

const phyloTree = new Phylogician();

declare global {
	// tslint:disable-next-line: interface-name
	interface Window {
		phyloTree: Phylogician;
	}
}

window.phyloTree = phyloTree;
