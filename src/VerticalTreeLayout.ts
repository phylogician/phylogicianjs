import * as d3 from 'd3';

import { Tree } from './Tree';
import { IsvgParams, TreeLayout } from './TreeLayout';

export class Vertical extends TreeLayout {
	constructor(tree: Tree, svgParams: IsvgParams) {
		super(tree, svgParams);
	}

	public calcTree(scaling: boolean): void {
		super
			.calcLabelDims()
			.calcPosX(scaling)
			.calcPosY()
			.calcBranchPath();
	}

	public calcPosX(scaling: boolean = false): Vertical {
		const longestLabel = this.getLongestLabel();
		if (scaling) {
			const xDom = d3
				.scaleLinear()
				.domain([0, this.tree.getMaxDistanceToRoot()])
				.range([this.svgParams.margin, this.svgParams.width - this.svgParams.margin - longestLabel]);
			this.tree.nodes.forEach(node => {
				const id = node.id;
				const posX = xDom(this.tree.getDistanceToRoot(id));
				node.setPosX(posX);
			});
		} else {
			const maxHops = this.tree.getMaxHopsToRoot();
			const xDom = d3
				.scaleLinear()
				.domain([maxHops, 0])
				.range([this.svgParams.margin, this.svgParams.width - this.svgParams.margin - longestLabel]);
			const nodeListCounter = this.tree.getAllMaxHopsToChildLeafs();
			this.tree.nodes.forEach(node => {
				const nodeId = node.id;
				if (nodeId) {
					node.setPosX(xDom(nodeListCounter[nodeId]));
				}
			});
		}
		return this;
	}

	public calcPosY(): Vertical {
		const leaves = this.tree.getLeafIds();
		const yDom = d3
			.scaleLinear()
			.domain([0, this.tree.getNumberOfLeafs() - 1])
			.range([this.svgParams.margin, this.svgParams.height - this.svgParams.margin]);
		// const spacing = (this.height - this.margin * 2) / leaves.length
		leaves.forEach((leaf, i) => this.tree.getNode(leaf).setPosY(yDom(i)));
		const internalIds = this.tree.selectInternalIds().reverse();
		internalIds.forEach(internalId => {
			const internal = this.tree.getNode(internalId);
			const childrenPos: number[] = [];
			internal.children.forEach(child => {
				childrenPos.push(this.tree.getNode(child).getPosY());
			});
			internal.setPosY(childrenPos.reduce((sum, pos) => sum + pos) / childrenPos.length);
		});
		return this;
	}

	// This should be part of the VerticalTreeLayout.
	public calcBranchPath(): Vertical {
		this.tree.nodes.forEach(node => {
			const parentId = node.getParentNodeId();
			if (parentId !== null) {
				const branchCoords: Array<[number, number]> = [];
				branchCoords.push([this.tree.getNode(parentId).getPosX(), this.tree.getNode(parentId).getPosY()]);
				branchCoords.push([node.getPosX(), node.getPosY()]);
				const branch = d3
					.line()
					.x((d: any) => d[0])
					.y((d: any) => d[1])
					.curve(d3.curveStepBefore);

				node.setBranchPath(branch(branchCoords));
			}
		});
		return this;
	}
}
