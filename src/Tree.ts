import * as newick from 'newick-reader';
import { TreeNode } from './TreeNode';

export class Tree {
	public nodes: TreeNode[];
	private numberOfNodes: number;

	constructor() {
		this.nodes = [];
		this.numberOfNodes = 0;
	}

	/**
	 * Builds a Tree from newick string
	 *
	 * @param {string} data
	 * @memberof Tree
	 */
	public buildTree(data: string): void {
		const treeObject = newick.read(data);
		this.makeTree(treeObject);
		this.setAsRoot(0);
	}

	/**
	 * Returns the maximum hops to child leafs to all nodes.
	 *
	 * @returns {number[]}
	 * @memberof Tree
	 */
	public getAllMaxHopsToChildLeafs(): number[] {
		const maxHops = this.getMaxHopsToRoot();
		const nodeListCounter: number[] = this.nodes.map(node => 0);
		const round: number = 1;
		const scanTree = (nodeIds: Set<number>, counter: number[], r: number) => {
			const parentsToNextRound: Set<number> = new Set();
			nodeIds.forEach(id => {
				const parentId = this.getNode(id).getParentNodeId();
				if (parentId !== null) {
					parentsToNextRound.add(parentId);
					counter[parentId] = r;
				}
			});
			if (r < maxHops) {
				scanTree(parentsToNextRound, counter, r + 1);
			}
		};
		const leafs = new Set(this.getLeafIds());
		scanTree(leafs, nodeListCounter, round);
		return nodeListCounter;
	}

	/**
	 * Returns the sum of branch lengths from the node to the root
	 *
	 * @param {number} nodeId
	 * @returns {number}
	 * @memberof Tree
	 */
	public getDistanceToRoot(nodeId: number): number {
		const node = this.getNode(nodeId);
		let length = node.branchLength || 0;
		if (node.getParentNodeId() !== null) {
			const parentNodeId = node.getParentNodeId();
			if (parentNodeId !== null) {
				length += this.getDistanceToRoot(parentNodeId);
			}
		}
		return length;
	}

	/**
	 * Returns the number of hops from node to root
	 *
	 * @param {number} nodeId
	 * @returns {number}
	 * @memberof Tree
	 */
	public getHopsToRoot(nodeId: number): number {
		let hops: number = 1;
		const node = this.getNode(nodeId);
		if (node.getParentNodeId() !== null) {
			const parentNodeId = node.getParentNodeId();
			if (parentNodeId !== null) {
				hops += this.getHopsToRoot(parentNodeId);
			}
		} else {
			return 0;
		}
		return hops;
	}

	/**
	 * Returns the maximum distance to the root.
	 *
	 * @returns {number}
	 * @memberof Tree
	 */
	public getMaxDistanceToRoot(): number {
		const leafs = this.getLeafIds();
		let maxDist = 0;
		leafs.forEach(leaf => {
			const dist = this.getDistanceToRoot(leaf);
			maxDist = maxDist < dist ? dist : maxDist;
		});
		return maxDist;
	}

	/**
	 * Returns maximum number of node hops to the root.
	 *
	 * @returns {number}
	 * @memberof Tree
	 */
	public getMaxHopsToRoot(): number {
		const leafs = this.getLeafIds();
		let maxHops = 0;
		leafs.forEach(leaf => {
			const hops = this.getHopsToRoot(leaf);
			maxHops = maxHops < hops ? hops : maxHops;
		});
		return maxHops;
	}

	/**
	 * Get TreeNode with the given id
	 *
	 * @param {number} id
	 * @returns {TreeNode}
	 * @memberof Tree
	 */
	public getNode(id: number): TreeNode {
		const selected = this.nodes.filter(n => n.id === id);
		if (selected.length === 0) {
			throw new Error(`Id ${id} not found.`);
		}
		if (selected.length > 1) {
			throw new Error(`More than one node with the same Id ${id}. Something is wrong.`);
		}
		return selected[0];
	}

	public findNodeByName(name: string): TreeNode[] {
		return this.nodes.filter(n => n.name === name);
	}

	/**
	 * Returns how many leafs the tree has.
	 *
	 * @returns {number}
	 * @memberof Tree
	 */
	public getNumberOfLeafs(): number {
		return this.nodes.filter(node => node.isLeaf()).length;
	}

	/**
	 * Return root node
	 *
	 * @private
	 * @returns {TreeNode}
	 * @memberof Tree
	 */
	public getRootNode(): TreeNode | null {
		const root = this.nodes.filter(node => node.isRoot());
		if (root.length === 1) {
			return root[0];
		}
		if (root.length > 1) {
			throw new Error(`There are ${root.length} roots with ids [${root.map(r => r.id).join(', ')}] assigned.`);
		}
		return null;
	}

	/**
	 * Return array of node ids of leafs
	 *
	 * @returns {number[]}
	 * @memberof Tree
	 */
	public getLeafIds(): number[] {
		const leafIds = this.nodes.filter((d: TreeNode) => d.isLeaf()).map(node => node.id);
		return leafIds;
	}

	/**
	 * REturn array of node ids of internal nodes
	 *
	 * @returns {number[]}
	 * @memberof Tree
	 */
	public selectInternalIds(): number[] {
		const internalIds = this.nodes.filter((d: TreeNode) => !d.isLeaf()).map(node => node.id);
		return internalIds;
	}

	/**
	 * Sets the node with Id passed as root. Avoids setting 2 root nodes.
	 *
	 * @param {number} id
	 * @memberof Tree
	 */
	public setAsRoot(id: number): void {
		const oldRoot = this.getRootNode();
		if (oldRoot === null) {
			this.getNode(id).setAsRoot();
		} else if (oldRoot.id !== id) {
			oldRoot.unsetAsRoot();
			this.getNode(id).setAsRoot();
		}
	}

	/**
	 * Builds the this.nodes array by scanning the treeObject
	 *
	 * @private
	 * @param {newick.ITree} treeObject
	 * @returns {TreeNode}
	 * @memberof Tree
	 */
	private makeTree(treeObject: newick.ITree): TreeNode {
		const treeNode = new TreeNode(treeObject.name, treeObject.branchLength, this.numberOfNodes);
		this.numberOfNodes++;
		this.nodes.push(treeNode);
		const children = [];
		if (treeObject.children.length) {
			treeObject.children.forEach((child: newick.ITree) => {
				const newchild = this.makeTree(child);
				const parentId = treeNode.id;
				if (parentId !== null) {
					newchild.setParentNodeId(parentId);
				}
				const childId = newchild.id;
				if (childId !== null) {
					treeNode.children.push(childId);
				}
			});
		}
		return treeNode;
	}
}
