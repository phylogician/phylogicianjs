interface Ipos {
	x: number;
	y: number;
}

interface Idim {
	h: number;
	w: number;
}

interface Ilabel {
	dim: Idim;
	font: string;
	color: string;
}

interface Inode {
	shape: string;
	color: string;
	size: number;
}

interface IvizOptions {
	node: Inode;
	label: Ilabel;
	pos: Ipos;
	branchPath: string;
}

export class TreeNode {
	public name: string;
	public branchLength: number | null;
	public children: number[];
	public parent: number | null;
	public readonly id: number;

	private root: boolean;

	private viz: IvizOptions;

	/**
	 * Creates an instance of TreeNode.
	 * @param {string} name
	 * @param {(number|null)} branchLength
	 * @memberof TreeNode
	 */
	constructor(name: string, branchLength: number | null, id: number) {
		this.name = name;
		this.branchLength = branchLength;
		this.children = [];
		this.id = id;
		this.parent = null;
		this.root = false;
		this.viz = {
			branchPath: '',
			label: {
				color: 'black',
				dim: {
					h: 0,
					w: 0,
				},
				font: '',
			},
			node: {
				color: 'black',
				shape: 'circle',
				size: 3,
			},
			pos: {
				x: 0,
				y: 0,
			},
		};
	}

	/**
	 * Sets parent to the Tree node.
	 *
	 * @param {Tree} parent
	 * @returns {TreeNode}
	 * @memberof Tree
	 */
	public setParentNodeId(parent: number): TreeNode {
		this.parent = parent;
		return this;
	}

	/**
	 * Returns parent of the node
	 *
	 * @returns {(number | null)}
	 * @memberof TreeNode
	 */
	public getParentNodeId(): number | null {
		return this.parent;
	}

	/**
	 * Returns true if the node is leaf
	 *
	 * @returns {boolean}
	 * @memberof TreeNode
	 */
	public isLeaf(): boolean {
		return this.children.length === 0 ? true : false;
	}

	/**
	 * Sets node to be root
	 *
	 * @memberof TreeNode
	 */
	public setAsRoot(): void {
		this.root = true;
	}

	/**
	 * Sets node to not be root
	 *
	 * @memberof TreeNode
	 */
	public unsetAsRoot(): void {
		this.root = false;
	}

	/**
	 *
	 *
	 * @returns {boolean}
	 * @memberof TreeNode
	 */
	public isRoot(): boolean {
		return this.root;
	}

	/**
	 * Returns the size of the node
	 *
	 * @returns {number}
	 * @memberof TreeNode
	 */
	public getNodeSize(): number {
		return this.viz.node.size;
	}

	/**
	 * Changes the node size
	 *
	 * @param {number} size
	 * @memberof TreeNode
	 */
	public setNodeSize(size: number): void {
		this.viz.node.size = size;
	}

	/**
	 * Returns the color of the node
	 *
	 * @returns {string} node color
	 * @memberof TreeNode
	 */
	public getNodeColor(): string {
		return this.viz.node.color;
	}

	/**
	 * Changes the node color.
	 *
	 * @param {string} color
	 * @memberof TreeNode
	 */
	public setNodeColor(color: string): void {
		this.viz.node.color = color;
	}

	/**
	 * Returns the shape of the node
	 *
	 * @returns {string} node shape
	 * @memberof TreeNode
	 */
	public getNodeShape(): string {
		return this.viz.node.shape;
	}

	/**
	 * Changes the node shape.
	 *
	 * @param {string} shape
	 * @memberof TreeNode
	 */
	public setNodeShape(shape: string): void {
		this.viz.node.shape = shape;
	}

	/**
	 * Returns the position X of the node
	 *
	 * @returns {number} position X
	 * @memberof TreeNode
	 */
	public getPosX(): number {
		return this.viz.pos.x;
	}

	/**
	 * Changes the position X of the node.
	 *
	 * @param {number} x
	 * @memberof TreeNode
	 */
	public setPosX(x: number): void {
		this.viz.pos.x = x;
	}

	/**
	 * Returns the position Y of the node
	 *
	 * @returns {number} position Y
	 * @memberof TreeNode
	 */
	public getPosY(): number {
		return this.viz.pos.y;
	}

	/**
	 * Changes the position Y of the node.
	 *
	 * @param {number} y
	 * @memberof TreeNode
	 */
	public setPosY(y: number): void {
		this.viz.pos.y = y;
	}

	/**
	 * Returns the width of the label of the node
	 *
	 * @returns {number}
	 * @memberof TreeNode
	 */
	public getLabelWidth(): number {
		return this.viz.label.dim.w;
	}

	/**
	 * Sets the width of the label of the node
	 *
	 * @param {number} width
	 * @memberof TreeNode
	 */
	public setLabelWidth(width: number): void {
		this.viz.label.dim.w = width;
	}

	/**
	 * Returns the height of the label of the node
	 *
	 * @returns {number}
	 * @memberof TreeNode
	 */
	public getLabelHeight(): number {
		return this.viz.label.dim.h;
	}

	/**
	 * Sets the height of the label of the node
	 *
	 * @param {number} height
	 * @memberof TreeNode
	 */
	public setLabelHeight(height: number): void {
		this.viz.label.dim.h = height;
	}

	/**
	 * Returns the font of the label of the node
	 *
	 * @returns {string}
	 * @memberof TreeNode
	 */
	public getLabelFont(): string {
		return this.viz.label.font;
	}

	/**
	 * Sets the font of the label of the node
	 *
	 * @param {string} font
	 * @memberof TreeNode
	 */
	public setLabelFont(font: string): void {
		this.viz.label.font = font;
	}

	/**
	 * Sets the branch path of the node.
	 *
	 * @param {string} path
	 * @memberof TreeNode
	 */
	public setBranchPath(path: string | null): void {
		this.viz.branchPath = path || '';
	}

	/**
	 * Gets the branch path of the node.
	 *
	 * @returns {string}
	 * @memberof TreeNode
	 */
	public getBranchPath(): string {
		return this.viz.branchPath;
	}
}
